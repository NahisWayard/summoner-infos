var express = require('express')
var app = express()
var http = require('http')
var https = require('https')
var server = http.createServer(app)
var io = require('socket.io')(server)
var request = require('request')
var fs = require('fs')
var PORT = 80
var APIKEY = '02e9cd64-18ba-4f9d-a19c-b4adc008881f'

function updateChampions() {
  if (!fs.existsSync('requests/')) {
    var mkdirRequest = true
    fs.mkdir('requests/')
  }
  request('https://global.api.pvp.net/api/lol/static-data/euw/v1.2/versions?api_key=' + APIKEY, function(error, response, body) {
    var lastVersion = JSON.parse(body)[0]
    fs.readFile('requests/lastVersion.json', "utf8", function(err, content) {
      if (lastVersion != content) {
        fs.writeFile('requests/lastVersion.json', lastVersion)
        request('https://global.api.pvp.net/api/lol/static-data/euw/v1.2/champion?locale=fr_FR&dataById=true&champData=spells,tags,passive,info&api_key=' + APIKEY, function(error, response, body) {
          console.log('Performing request')
          fs.writeFile('requests/champions.json', body)
        })
      }
    })
  })
  if (mkdirRequest) {
    return true
  }
}

app.use(express.static(__dirname + '/public'))

app.get('/', function(req, res, next) {
  res.render('index.ejs')
})

app.get('/champions', function(req, res, next) {
  updateChampions()
  fs.readFile('requests/champions.json', "utf8", function(err, content) {
    res.render('champions.ejs', {data: content})
  })
})


app.get('/:region/summoner/', function(req, res, next) {
  res.statusCode = 302
  res.setHeader('Location', '/' + req.params.region + '/summoner/' + req.query.pseudo)
  res.end()
})

app.get('/:region/summoner/:pseudo', function(req, res, next) {
  var region = req.params.region
  var pseudo = req.params.pseudo

  request('https://' + region + '.api.pvp.net/api/lol/' + region + '/v1.4/summoner/by-name/' + pseudo + '?api_key=' + APIKEY, function(error, response, body) {
    if (response.statusCode !== 200) {
      return res.render('error.ejs', {
        statusCode: response.statusCode
      })
    } else
      res.render('summoner.ejs', {
        basicInfos: body,
        region: region
      })
  })
})

io.sockets.on('connection', function(socket) {
  socket.on('userData', function(data) {
    request('https://' + data.region + '.api.pvp.net/api/lol/' + data.region + '/v2.5/league/by-summoner/' + data.id + '/entry?api_key=' + APIKEY, function(error, res, body) {
      if (res.statusCode != 200) {
        socket.emit('leagueEntry', res.statusCode)
      } else {
        socket.emit('leagueEntry', JSON.parse(body))
      }
    })
    request('https://' + data.region + '.api.pvp.net/api/lol/' + data.region + '/v1.3/stats/by-summoner/' + data.id + '/ranked?api_key=' + APIKEY, function(error, response, body) {
      socket.emit('champsStats', JSON.parse(body))
    })
    request('https://' + data.region + '.api.pvp.net/api/lol/' + data.region + '/v2.2/matchlist/by-summoner/' + data.id + '?api_key=' + APIKEY, function(error, response, body) {
      socket.emit('matchHistory', JSON.parse(body))
    })
  })
})

server.listen(PORT)
